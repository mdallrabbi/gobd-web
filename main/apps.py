from django.apps import AppConfig


# from django.contrib.auth import get_user_model
# from django.db.models.signals import post_save
# from django.utils.translation import ugettext_lazy as _
# from main.signals import create_store_profile, save_store_profile, create_delivery_boy_profile, save_delivery_boy_profile

# User = get_user_model()

class MainConfig(AppConfig):
    name = 'main'

    # def ready(self):
    #     post_save.connect(create_store_profile, sender=User)
    #     post_save.connect(save_store_profile, sender=User)
    #     post_save.connect(create_delivery_boy_profile, sender=User)
    #     post_save.connect(save_delivery_boy_profile, sender=User)
