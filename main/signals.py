# from django.contrib.auth import get_user_model
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from main.models import Store, DeliveryBoy
#
# User = get_user_model()
#
#
# @receiver(post_save, sender=User)
# def create_store_profile(sender, instance, created, **kwargs):
#     if created:
#         Store.objects.create(user=instance)
#
#
# @receiver(post_save, sender=User)
# def save_store_profile(sender, instance, **kwargs):
#     instance.store.save()
#
#
# @receiver(post_save, sender=User)
# def create_delivery_boy_profile(sender, instance, created, **kwargs):
#     if created:
#         DeliveryBoy.objects.create(user=instance)
#
#
# @receiver(post_save, sender=User)
# def save_delivery_boy_profile(sender, instance, **kwargs):
#     instance.delivery_boy.save()